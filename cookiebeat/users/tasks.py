from cookiebeat.taskapp.celery import app
from .models import CeleryCounter


@app.task(bind=True)
def increment_counter(self):
    counter, _ = CeleryCounter.objects.get_or_create(id=1)
    counter.counter += 1
    counter.save(update_fields=['counter'])
    print(counter)
    return counter
