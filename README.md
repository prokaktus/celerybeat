cookiebeat
==========



## Celery


This app comes with Celery.

To run a celery worker:

```
$ cd cookiebeat
$ celery -A cookiebeat.taskapp worker --loglevel=info
```

Then you need to set up beat frequency. You should run celery beat process for that
```
$ celery -A cookiebeat.taskapp beat --loglevel=info --max-interval 30 # you may adapt settings, of course
```

If you need status or inspect queries, you could run:
```
$ celery -A cookiebeat.taskapp inspect scheduled
$ celery -A cookiebeat.taskapp inspect active
$ celery -A cookiebeat.taskapp status
```

Please note: For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with `manage.py`, you should be right.

For local development, you may set `CELERY_ALWAYS_EAGER = True` in `settings`. It will force synchronous tasks execution (pretty convenient for development, but must be turn off in production).

